let numero = 0
console.log("REPETICAO WHILE")
while(numero<=10){

    if(numero%2==0){
        console.log(`Valor nr: ${(numero)} é par!`)
    }else{
        console.log(`valor nr: ${(numero)}`)
    }
    numero++;
}

console.log("REPETICAO DO/WHILE")
let numero1 = 0

do {
    console.log(`Valor nr: ${numero1}`)
    numero1++;
} while (numero1<=10);

console.log("REPETICAO FOR")
for(let numero2 = 0; numero2<=10; numero2++){
    if(numero%2==0){
        console.log(`Valor nr: ${(numero2)} é par!`)
    }else{
        console.log(`valor nr: ${(numero2)}`)
    }
}